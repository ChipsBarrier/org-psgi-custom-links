package Org::Export::Html::Link::Man;


#** @class Org::Export::Html::Link::Man
#
# Translate [[man:...]] Links to HTML.  Depends on a org-protocol Handler.
#
# @author Sebastian Rose <sebastian_rose@gmx.de>
#*


use strict;
use utf8;
use Moose;
use Org::Export::CustomLink;
use Org::Export::Renderer;

extends 'Org::Export::CustomLink';
with    'Org::Export::Renderer';

sub format {

    my $self = shift;

    sprintf '<a href="org-protocol://man://%s">%s</a>',
        $self->fullpath,
        $self->getLabel;
}

__PACKAGE__->meta->make_immutable;

1;
