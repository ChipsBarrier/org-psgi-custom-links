package Org::Export::Html::Link::Ticket;


#** @class Org::Export::Html::Link::Ticket
#
# Render [[ticket:...][...]] Links on HTML Export.  Links will point to
# https://your.redmine-installation.tld/issues/TICKET-ID.
#
# @author Sebastian Rose <sebastian_rose@gmx.de>
#*


use strict;
use utf8;
use Moose;
use Org::Export::CustomLink;
use Org::Export::Renderer;


extends 'Org::Export::CustomLink';
with    'Org::Export::Renderer';



sub format {

    my $self = shift;
    ( my $href = $self->fullpath ) =~ s /^#//;

    sprintf(
        '<a target="_blank" href="https://YOUR.REDMINE-INSTALLATION.com/issues/%s">%s</a>',
        $href,
        $self->getLabel
        );
}

__PACKAGE__->meta->make_immutable;

1;
