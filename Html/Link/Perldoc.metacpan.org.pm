package Org::Export::Html::Link::Perldoc;


#** @class Org::Export::Html::Link::Perldoc
#
# Render [[perldoc:...][...]] Links on HTML Export.  Links will point to
# metacpan.org.
#
# @author Sebastian Rose <sebastian_rose@gmx.de>
#*


use strict;
use utf8;
use Moose;
use Org::Export::CustomLink;
use Org::Export::Renderer;


extends 'Org::Export::CustomLink';
with    'Org::Export::Renderer';



sub format {

    my $self = shift;
    sprintf(
        '<a target="_blank" href="https://metacpan.org/pod/%s">%s</a>',
        $self->link->fullpath,
        $self->getLabel
        );
}

__PACKAGE__->meta->make_immutable;

1;
