package Org::Export::Html::Link::Info;


#** @class Org::Export::Html::Link::Info
#
# Translate [[info:...]] Links to HTML.  Depends on a org-protocol Handler.
#
# @author Sebastian Rose <sebastian_rose@gmx.de>
#*


use strict;
use utf8;
use Moose;
use Org::Export::CustomLink;
use Org::Export::Renderer;

extends 'Org::Export::CustomLink';
with    'Org::Export::Renderer';

sub format {

    my $self = shift;
    my $p = '('.$self->link->path;
    $p =~ s/#|$/) /;                # Exchange the first space or end of string.
    my $label = $self->getLabel;
    return "<a href=\"org-protocol://info://$p\">$label</a>";
}


1;
