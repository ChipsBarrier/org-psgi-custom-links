package Org::Export::Html::Link::Man;


#** @class Org::Export::Html::Link::Man
#
# Translate [[man:...]] Links to HTML. Points to linux.die.net.
#
# @author Sebastian Rose <sebastian_rose@gmx.de>
#*


use strict;
use utf8;
use Moose;
use Org::Export::CustomLink;
use Org::Export::Renderer;

extends 'Org::Export::CustomLink';
with    'Org::Export::Renderer';

sub format {

    my $self = shift;

    my( $man, $n ) = reverse split(' ', $self->fullpath, 2);
    $n ||= 1;

    sprintf '<a target=\"_blank\" href="https://linux.die.net/man/%d/%s">%s</a>',
        $n,
        $man,
        $self->getLabel;

}

__PACKAGE__->meta->make_immutable;

1;
