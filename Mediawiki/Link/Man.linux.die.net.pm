package Org::Export::Mediawiki::Link::Man;


#** @class Org::Export::Mediawiki::Link::Man
#
# Translate [[man:...]] Links to MediaWiki.  Links to http://linux.die.net/man/.
#
# @author Sebastian Rose <sebastian_rose@gmx.de>
#*


use strict;
use utf8;
use Moose;
use Org::Export::CustomLink;
use Org::Export::Renderer;

extends 'Org::Export::CustomLink';
with    'Org::Export::Renderer';

sub format {

    my $self = shift;

    my $label = $self->getLabel;
    my($page, $sec) = $self->fullpath =~ /([-\w\d]+)\s*(?:\((\d)\))?/;

    sprintf '[http://linux.die.net/man/%s%s %s]',
        ( $sec ? "$sec/" : '' ),
        $page,
        $label;

}

__PACKAGE__->meta->make_immutable;

1;
