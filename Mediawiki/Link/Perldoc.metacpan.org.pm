package Org::Export::Mediawiki::Link::Perldoc;


#** @class Org::Export::Mediawiki::Link::Perldoc
#
# Render [[perldoc:...][...]] Links for MediaWiki Export.  Links will point to
# metacpan.org.
#
# @author Sebastian Rose <sebastian_rose@gmx.de>
#*


use strict;
use utf8;
use Moose;
use Org::Export::CustomLink;
use Org::Export::Renderer;


extends 'Org::Export::CustomLink';
with    'Org::Export::Renderer';



sub format {

    my $self = shift;
    sprintf(
        '[https://metacpan.org/pod/%s %s]',
        $self->fullpath,
        $self->getLabel
        );
}

__PACKAGE__->meta->make_immutable;

1;
