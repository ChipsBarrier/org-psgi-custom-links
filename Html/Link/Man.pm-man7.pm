package Org::Export::Html::Link::Man;


#** @class Org::Export::Html::Link::Man
#
# Translate [[man:...]] Links to HTML.  Depends on a org-protocol Handler.
#
# @author Sebastian Rose <sebastian_rose@gmx.de>
#*


use strict;
use utf8;
use Moose;
use Org::Link;
use Org::Export::Renderer;

extends 'Org::Export::CustomLink';
with    'Org::Export::Renderer';

sub format {

    my $self = shift;

    my( $man, $n ) = reverse split(' ', $self->link->fullpath, 2);
    $n ||= 1;

    my $label = $self->getLabel;

    sprintf '<a target=\"_blank\" href="http://man7.org/linux/man-pages/man%d/%s.%d.html">%s</a>',
        $n,
        $man,
        $n,
        $label;

}

__PACKAGE__->meta->make_immutable;

1;
