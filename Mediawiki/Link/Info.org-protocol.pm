package Org::Export::Mediawiki::Link::Info;


#** @class Org::Export::Mediawiki::Link::Info
#
# Translate [[info:...]] Links to MediaWiki.  Depends on a org-protocol Handler.
#
# @author Sebastian Rose <sebastian_rose@gmx.de>
#*


use strict;
use utf8;
use Moose;
use Org::Export::CustomLink;
use Org::Export::Renderer;


extends 'Org::Export::CustomLink';
with    'Org::Export::Renderer';



sub format {

    my $self = shift;
    my $p = '(' . $self->path;
    $p =~ s/#|$/) /;                # Exchange the first space or end of string.
    my $label = $self->getLabel;
    # NOTE:  Custom URIs might be disabled in the target wiki!
    return "[org-protocol://info://$p $label]";
}

__PACKAGE__->meta->make_immutable;

1;
