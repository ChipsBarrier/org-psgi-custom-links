;;; org-redmine-ticket-link - Support for links to redmine tickets

(require 'org)


(org-add-link-type
 "ticket"
 'org-redmine-ticket-open
 'org-redmine-ticket-export)


(add-hook 'org-store-link-functions 'org-redmine-ticket-store-link)

(defcustom org-redmine-ticket-command 'browse-url
  "The Emacs command to be used to display a redmine ticket."
  :group 'org-link
  :type '(choice (const browse-url)))

(defcustom org-redmine-ticket-base-url "https://YOUR.REDMINE-INSTALLATION.com/issues/"
  "The Emacs command to be used to display a redmine ticket."
  :group 'org-link
  :type 'string
  )

(defcustom org-redmine-export-html-format "<a href=\"%s\" target=\"_blank\">%s</a>"
  "Format for Org mode's XHTML export of redmine tickets.
The format should contain two place holders.  The first one
for the url, the second one for the link description."
  :group 'org-link
  :type  'string)


(defcustom org-redmine-export-LaTeX-format
  "\\href{file://%u}{%d}"
  "Format used to export a redmine link: link to LaTeX.
Possible replacements:
 \"%u\"  -  the URL
 \"%d\"  -  link description"
  :group 'org-link
  :type  'string)

(defun org-redmine-ticket-open (path)
  "Visit a redmine redmine-ticket.
Argument PATH is the url part of the link without prefix."
  (let ((ticket
         (if (string-match "#?\\([0-9]+\\)" path)
             (match-string 1 path)
           nil)))
    (when (null ticket)
      (error "No ticket number found in %s" path))
    (funcall org-redmine-ticket-command
             (concat org-redmine-ticket-base-url
                     ticket))))

(defun org-redmine-ticket-export (path description format)
  "Export a link to a redmine ticket."
  (let* ((url (concat org-redmine-ticket-base-url
                      (substring-no-properties path 1)))
         (desc (or description path)))
    (cond
     ((eq format 'html)
      (format org-redmine-export-html-format url desc))
     ((eq format 'latex)
      (org-replace-escapes
       org-redmine-export-LaTeX-format
       (list (cons "%u" url)
             (cons "%d" desc))))
     (t
      url))))

(defun org-redmine-ticket-store-link ()
  "Extract the ticket number name from the link url."
  (message "org-redmine-ticket-store-link NOT IMPLEMENTED"))




(provide 'org-redmine-ticket-link)
