package Org::Export::Html::Link::Docbase;


#** @class Org::Export::Html::Link::Docbase
#
# Render [[docbase:...][...]] Links on HTML Export.  Links will point to the
# location of your locally installed documentation (=/usr/share/doc= in
# Debian/Ubuntu). Note though, that those links will not work, unless that
# location is linked under webroot AND =/usr/share/doc= is added to your
# =include.directories= (see the =<org>= section in =etc/orgweb.rc=).
#
# @author Sebastian Rose <sebastian_rose@gmx.de>
#*


use strict;
use utf8;
use Moose;
use Org::Export::CustomLink;
use Org::Export::Renderer;


extends 'Org::Export::CustomLink';
with    'Org::Export::Renderer';



sub format {

    my $self = shift;
    my $base_href = '/org/local/debian_documentation';
    sprintf(
        "<a target=\"_blank\" href=\"$base_href/%s\">%s</a>",
        $self->link->fullpath,
        $self->getLabel
        );
}

__PACKAGE__->meta->make_immutable;

1;
